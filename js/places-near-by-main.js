"use strict";

/**
 * @file
 * Attaches behaviors for the Near-By Google Maps.
 */
(function ($, Drupal, drupalSettings) {

  //Intializing variables.
  var latitude, longitude, radius, placesdata = [], placeslocation, infoWindow = null;
  var index = 0, fixedlength = 5, listsize = 5, markers = [];
  const placesList = document.getElementById("places-near-by-map-list-data");

  // Initializes the map with lat and long value along with markers.
  function initplaces() {

    var location = drupalSettings.places_near_by.location;

    latitude = parseFloat(Number(location.latitude));
    longitude = parseFloat(Number(location.longitude));
    radius = parseFloat(Number(location.radius));

    location = new google.maps.LatLng(latitude, longitude);

    map = new google.maps.Map(document.getElementById('places-near-by-map-block'), {
      center: location,
      zoom: 16
    });

    // Create the places service.
    var service = new google.maps.places.PlacesService(map);

    // Perform a nearby search.
    service.nearbySearch({
        location: location,
        radius: radius
      },

      function (results, status, pagination) {
        if (status !== 'OK') {
          return;
        }
        if (pagination.hasNextPage) {
          pagination.nextPage();
        }
        placesdata = placesdata.concat(results);
        placeslocation = location;
        
        if (placesdata.length < fixedlength) {
          fixedlength = placesdata.length;
        }

        if (!pagination.hasNextPage) {
          $('.places-near-by-lds-hourglass').addClass('hide')
          $('.places-near-by-left-section').removeClass('hide');
          $('#current-places-near-by-map-pagination').text((index + 1) + ' - ' + fixedlength + ' of ' + placesdata.length);
          createMarkers(results.slice(index, listsize), location);
        }
      });
  }

  //Creates the markers based on places result.
  function createMarkers(places,location) {
    var bounds = new google.maps.LatLngBounds();
    infoWindow = new google.maps.InfoWindow({
      content: name
    });

    google.maps.event.addListener(infoWindow, 'closeclick', function () {
      removesClassFromMarkers();
    });

    var icon = {
      url: "/modules/contrib/places_near_by/assets/images/map_home.svg",
      scaledSize: new google.maps.Size(40, 40)
    };

    var propertyMarker = new mapIcons.Marker({
      map: map,
      position: {
        lat: latitude,
        lng: longitude
      },
      icon: icon
    });

    for (var i = 0, place, subIndex = index; place = places[i]; i++, subIndex++) {
      var distance = google.maps.geometry.spherical.computeDistanceBetween(location, places[i].geometry.location);
      distance = distance * 0.000621;
      distance = distance.toFixed(2);
      placesList.innerHTML += '<li data-location=' + subIndex + '><div class="nearby-places-list-wrapper"><span class="nearby-places-name">' + place.name + '</span><span class="nearby-places-distance">' + distance + ' mi ' + '</span></div></li>';
      bounds.extend(place.geometry.location);
      var markerData = findTheIcon(places[i].types);
      var marker = new mapIcons.Marker({
        map: map,
        position: places[i].geometry.location,
        icon: markerData.icon,
        map_icon_label: markerData.label,
      });

      markers.push(marker);
      marker.addListener('click', (function (marker, i, infowindow) {
        return function () {
          var htmlData = '<div class="maps-content">' +
            '<h3>' + places[i].name + '</h3>' +
            '<br>' + places[i].vicinity +
            '<br>' + distance + 'mi away from community' +
            '</div>';

          infowindow.setContent(htmlData);
          infowindow.open(map, marker);
          removesClassFromMarkers();
          placesList.getElementsByTagName('li')[i].classList.add("places-near-by-li-element");
        }
      })(marker, i, infoWindow));
    }
    bounds.extend(new google.maps.LatLng(latitude, longitude));
    map.fitBounds(bounds);
  }

  function findTheIcon(icontypes) {
    for (let i = 0; i < icontypes.length; i++) {
      if(icontypes[i] in config){
        config[icontypes[i]].icon['scale'] = 0.6;
        return {
          'icon': config[icontypes[i]].icon,
          'label': config[icontypes[i]].map_icon_label
        };
      }
    }
  }

  // Sets the map on all markers in the array.
  function setMapOnAll(map) {
    for (let i = 0; i < markers.length; i++) {
      markers[i].setMap(map);
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  function clearMarkers() {
    setMapOnAll(null);
  }


  //Function which removes font-color.
  function removesClassFromMarkers() {
    //Removes the class on mouse out.
    for (var i = 0, place; place = placesList.getElementsByTagName('li')[i]; i++) {
      if (place.classList.contains("places-near-by-li-element")) {
        place.classList.remove('places-near-by-li-element');
      }
    }
  }

  //Onclick of prev button of map.
  $('.prev-page-button').click(function(event) {
    if(index > 0) {
      //Clears markers on map.
      clearMarkers();
      //Clearing li map list.
      $('#places-near-by-map-list-data').empty();
      $('#current-places-near-by-map-pagination').text(index+1 + ' of ' + (placesdata.length));
      index -= fixedlength;
      listsize -= fixedlength;
      $('#current-places-near-by-map-pagination').text((index+1) + ' - ' + (index+fixedlength) + ' of ' + placesdata.length);
      createMarkers(placesdata.slice(index, listsize), placeslocation);
    }
    else {
      $('#current-places-near-by-map-pagination').text( 1 + ' - ' + fixedlength + ' of ' + placesdata.length);
    }
  });

  //Onclick of next button of map.
  $('.next-page-button').click(function(event) {
    if(placesdata.length > listsize) {

      //Clears markers on map.
      clearMarkers();

      //Clearing li map list.
      $('#places-near-by-map-list-data').empty();
      index += fixedlength;
      listsize += fixedlength;

      $('#current-places-near-by-map-pagination').text((index+1) + ' - ' + (index+fixedlength) + ' of ' + placesdata.length);
      createMarkers(placesdata.slice(index, listsize), placeslocation);
    }
    else {
      $('#current-places-near-by-map-pagination').text(((placesdata.length-fixedlength)+1) + ' - ' + placesdata.length + ' of ' + placesdata.length);
    }
  });

  $('#places-near-by-map-list-data').on('mouseover', 'li', function () {
    google.maps.event.trigger(markers[$(this).attr('data-location')], 'click');
  });

  $('#places-near-by-map-list-data').on('mouseout', 'li', function () {
    removesClassFromMarkers();
    infoWindow.close();
  });

  //Drupal behaviors.
  Drupal.behaviors.placesnearbyBehavior = {
    attach: function attach(context, settings) {
      // For avoiding calling behavior multiple times.
      if (context !== document) {
        return;
      }
      //Calls this function to search places nearby.
      initplaces();
    }
  };
})(jQuery, Drupal, drupalSettings);
