<?php

namespace Drupal\places_near_by\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure places near by settings for this site.
 */
class PlacesNearByConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'places_near_by.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'places_near_by_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['google_map_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google API Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('google_map_api_key'),
    ];

    $form['places_near_by_map_block'] = [
      '#type' => 'details',
      '#title' => $this->t('Places Near By Map Block Configuration'),
      '#open' => TRUE,
    ];

    $form['places_near_by_map_block']['places_near_by_lat'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Latitude'),
      '#required' => TRUE,
      '#default_value' => $config->get('places_near_by_lat'),
    ];

    $form['places_near_by_map_block']['places_near_by_log'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Longitude'),
      '#required' => TRUE,
      '#default_value' => $config->get('places_near_by_log'),
    ];

    $form['places_near_by_map_block']['places_near_by_radius'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the Radius distance in miles.'),
      '#default_value' => empty($config->get('places_near_by_radius')) ? '3' : $config->get('places_near_by_radius'),
      '#options' => array_combine(range(1, 10), range(1, 10)),
      '#description' => $this->t('This value is used to get the list of near by places within this limit.'),
    ];

    $form['places_near_by_places_list'] = [
      '#type' => 'details',
      '#title' => $this->t('Places Near By List Block Configuration'),
      '#open' => TRUE,
    ];

    $form['places_near_by_places_list']['places_near_by_main_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter Main text which appears on left side of block.'),
      '#default_value' => empty($config->get('places_near_by_main_text')) ? 'Explore places near by' : $config->get('places_near_by_main_text'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration and saves the configurations.
    if (!empty($form_state->getValue('google_map_api_key'))) {
      $this->configFactory->getEditable(static::SETTINGS)
        ->set('google_map_api_key', $form_state->getValue('google_map_api_key'))
        ->save();
    }

    if (!empty($form_state->getValue('places_near_by_lat'))) {
      $this->configFactory->getEditable(static::SETTINGS)
        ->set('places_near_by_lat', $form_state->getValue('places_near_by_lat'))
        ->save();
    }

    if (!empty($form_state->getValue('places_near_by_log'))) {
      $this->configFactory->getEditable(static::SETTINGS)
        ->set('places_near_by_log', $form_state->getValue('places_near_by_log'))
        ->save();
    }

    if (!empty($form_state->getValue('places_near_by_radius'))) {
      $this->configFactory->getEditable(static::SETTINGS)
        ->set('places_near_by_radius', $form_state->getValue('places_near_by_radius'))
        ->save();
      parent::submitForm($form, $form_state);
    }

    if (!empty($form_state->getValue('places_near_by_main_text'))) {
      $this->configFactory->getEditable(static::SETTINGS)
        ->set('places_near_by_main_text', $form_state->getValue('places_near_by_main_text'))
        ->save();
      parent::submitForm($form, $form_state);
    }

  }

}
