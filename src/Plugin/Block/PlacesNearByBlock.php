<?php

namespace Drupal\places_near_by\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\RendererInterface;

/**
 * Provides a 'places naer by' block.
 *
 * @Block(
 *  id = "places_near_by_block",
 *  admin_label = @Translation("Places near by"),
 * )
 */
class PlacesNearByBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The Variable which stores configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configfactory;

 /**
  * Drupal\Core\Render\RendererInterface definition.
  *
  * @var Drupal\Core\Render\RendererInterface
  */
  protected $renderer;

  /**
   * Constructor for the PlacesNearByBlock.
   *
   * @param array $configuration
   *   Configuration array.
   * @param string $plugin_id
   *   Block plugin id.
   * @param mixed $plugin_definition
   *   Block plugin defination.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory Interface.
   * @param Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configfactory = $config_factory;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->get('config.factory'),
          $container->get('renderer')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $res = [];
    $config = $this->configfactory->get('places_near_by.settings');
    $res['latitude'] = $config->get('places_near_by_lat');
    $res['longitude'] = $config->get('places_near_by_log');
    $res['radius'] = !empty($config->get('places_near_by_radius')) ? ($config->get('places_near_by_radius') * 1609.34) : (3 * 1609.34);
    $res['places_near_by_main_text'] = $config->get('places_near_by_main_text');

    $build = [
      '#theme' => 'places-near-by-block',
      '#attached' => [
        'library' => [
          'places_near_by/place_api',
          'places_near_by/main',
          'places_near_by/map-icons',
        ],
        'drupalSettings' => [
          'places_near_by' => [
            'location' => $res,
          ],
        ],
      ],
      '#term' => $res,
    ];

    // Merges the cache contexts, cache tags and max-age of the config object 
    // and user entity that the render array depend on.
    $this->renderer->addCacheableDependency($build, $config);

    return $build;

  }

}
